.. Rarecoal Documentation documentation master file, created by
   sphinx-quickstart on Fri Mar  3 15:28:00 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Rarecoal's Documentation!
==================================================

Rarecoal is a software to model the population history of large sets of genomic samples from different populations. The main tool is called rarecoal and is maintained in the rarecoal_ github repository. Additional tools to prepare data for usage in rarecoal are maintained in the rarecoal-tools_ github repository. The project has been first reported in our publication_ on a genetic analyses of ancient Anglo-Saxon genomes from England, and further developed as reported in a recent preprint_. The mathematical derivations can be found in the main github repository in the ``doc`` directory.

.. note:: This documentation is written for rarecoal version 1.5.0, and rarecoal-tools version 1.2.7.

.. _rarecoal: https://www.github.com/stschiff/rarecoal
.. _rarecoal-tools: https://www.github.com/stschiff/rarecoal-tools
.. _publication: http://www.nature.com/articles/ncomms10408
.. _preprint: https://www.biorxiv.org/content/early/2017/10/13/203018

Contents:

.. toctree::
   :maxdepth: 2

   installation
   rarecoal
   rarecoal-tools

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
