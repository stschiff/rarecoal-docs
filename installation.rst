Installation instructions
=========================

- Install the `Haskell tool stack`_. The recommended way is by simply downloading a binary for your platform.

- Download the latest release of the rarecoal_ repository, or clone it directly via ``git clone https://github.com/stschiff/rarecoal.git``. In the latter case you can also easily update the repository by executing ``git pull`` within the directory.

- ``cd`` into the downloaded folder.

- Run `stack build`. This will download all the dependencies, including the Haskell Compiler, and may take a while. You may get an error about missing software such as pkg-config, or gsl, which then need to be installed using your standard package manager, such as ``apt-get`` on Linux or ``brew`` on a Mac. As soon as you have installed those, just run ``stack build`` again to continue where you left before the error.

- After building, the executables are buried relatively deep in the hidden ``.stack-work`` directory within the ``rarecoal`` directory. You can either find them in there or simply run ``stack install`` to copy the executables into "~/.local/bin". You should then add this directory to your path.

- Download the rarecoal-tools_ repository and install it using ``stack build`` and ``stack install`` exactly as described above for the rarecoal_ repository.

.. _Haskell tool stack: https://github.com/commercialhaskell/stack
.. _rarecoal-tools: https://github.com/stschiff/rarecoal-tools
.. _rarecoal: https://github.com/stschiff/rarecoal
